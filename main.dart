//Connection Manager
import 'dart:io'; //InternetAddress utility
import 'dart:async'; //For StreamController/Stream

import 'package:connectivity/connectivity.dart';

class ConnectionStatusSingleton {
  //This creates the single instance by calling the `_internal` constructor specified below
  static final ConnectionStatusSingleton _singleton = new ConnectionStatusSingleton._internal();
  ConnectionStatusSingleton._internal();

  //This is what's used to retrieve the instance through the app
  static ConnectionStatusSingleton getInstance() => _singleton;

  //This tracks the current connection status
  bool hasConnection = false;

  //This is how we'll allow subscribing to connection changes
  StreamController connectionChangeController = new StreamController.broadcast();

  //flutter_connectivity
  final Connectivity _connectivity = Connectivity();

  //Hook into flutter_connectivity's Stream to listen for changes
  //And check the connection status out of the gate
  void initialize() {
    _connectivity.onConnectivityChanged.listen(_connectionChange);
    checkConnection();
  }

  Stream get connectionChange => connectionChangeController.stream;

  //A clean up method to close our StreamController
  //   Because this is meant to exist through the entire application life cycle this isn't
  //   really an issue
  void dispose() {
    connectionChangeController.close();
  }

  //flutter_connectivity's listener
  void _connectionChange(ConnectivityResult result) {
    checkConnection();
  }

  //The test to actually see if there is a connection
  Future<bool> checkConnection() async {
    bool previousConnection = hasConnection;

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        hasConnection = true;
      } else {
        hasConnection = false;
      }
    } on SocketException catch(_) {
      hasConnection = false;
    }

    //The connection status changed send out an update to all listeners
    if (previousConnection != hasConnection) {
      connectionChangeController.add(hasConnection);
    }

    return hasConnection;
  }
}

//Post APi Calling
//signup model is model class

Future<signup_model> signup() async {
  // showLoaderDialog(context);
  print("CLICKED 123 ==");
  check().then((intenet) {
    if (intenet != null && intenet) {
      // Internet Present Case
      showLoaderDialog(context);
    }else{
      Fluttertoast.showToast(
          msg: "Please check your Internet connection!!!!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: const Color(0xffC83760),
          textColor: Colors.white,
          fontSize: 16.0);
    }

    // No-Internet Case
  });
  var response = await http.post('https://ctinfotech.com/CTCC/goinflu/api/signup', body: toMap());
  print(signup_model.fromJson(json.decode(response.body)).code);
  print(signup_model.fromJson(json.decode(response.body)).message);
  status = (signup_model.fromJson(json.decode(response.body)).code);
  message = (signup_model.fromJson(json.decode(response.body)).message);

  if(status=="1"){



    if(message=="Signup Succesfully!"){
      Navigator.pop(context);
      Fluttertoast.showToast(
          msg: message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: const Color(0xffC83760),
          textColor: Colors.white,
          fontSize: 16.0);


    }
  }else{
    Navigator.pop(context);
    print('else==============');
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: const Color(0xffC83760),
        textColor: Colors.white,
        fontSize: 16.0);
  }
  return signup_model.fromJson(json.decode(response.body));
}
Future<bool> check() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    return true;
  }
  return false;
}
Map toMap() {
  var map = new Map<String, dynamic>();
  map["full_name"] = txt_name.text.toString();
  map["email"] = txt_email.text.toString();
  map["password"] = txt_password.text.toString();
  map["user_type"] = user_type.toString();
  print("name ====");
  print( txt_name.text.toString());
  print("email ====");
  print(txt_email.text.toString());
  print("password ====");
  print(txt_password.text.toString());
  print("user_type ====");
  print(user_type.toString());
  return map;
}

//Get APi Calling
//influencer model is model class

Future<influencer_model> influencer_list() async {
  // showLoaderDialog(context);
  print("CLICKED 123 ==");
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var response = await http.get('https://ctinfotech.com/CTCC/goinflu/api/influencer_list');
  print(influencer_model.fromJson(json.decode(response.body)).code);
  print(influencer_model.fromJson(json.decode(response.body)).message);
  print(influencer_model.fromJson(json.decode(response.body)).influencerList);
  status = (influencer_model.fromJson(json.decode(response.body)).code);
  message = (influencer_model.fromJson(json.decode(response.body)).message);

  if(status=="1"){
    influencer.clear();
    // if(dataList.isEmpty){
    //   Fluttertoast.showToast(
    //       msg: "No data found",
    //       toastLength: Toast.LENGTH_SHORT,
    //       gravity: ToastGravity.BOTTOM,
    //       backgroundColor: Colors.green,
    //       textColor: Colors.white,
    //       fontSize: 16.0);
    //
    // }else{
    influencer.addAll(influencer_model.fromJson(json.decode(response.body)).influencerList);
    print(influencer.length);
    // }
    // prefs.remove('user_name');
    // prefs.remove('email');
  }else{

    print('else==============');
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0);
  }
  return influencer_model.fromJson(json.decode(response.body));
}


//Multi selection dialoge

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MultiSelectDialogItem<V> {
  const MultiSelectDialogItem(this.value, this.label);

  final V value;
  final String label;
}

class MultiSelectDialog<V> extends StatefulWidget {
  MultiSelectDialog({Key key, this.items, this.initialSelectedValues}) : super(key: key);

  final List<MultiSelectDialogItem<V>> items;
  final Set<V> initialSelectedValues;

  @override
  State<StatefulWidget> createState() => _MultiSelectDialogState<V>();
}

class _MultiSelectDialogState<V> extends State<MultiSelectDialog<V>> {
  final _selectedValues = Set<V>();

  void initState() {
    super.initState();
    if (widget.initialSelectedValues != null) {
      _selectedValues.addAll(widget.initialSelectedValues);
    }
  }

  void _onItemCheckedChange(V itemValue, bool checked) {
    setState(() {
      if (checked) {
        _selectedValues.add(itemValue);
      } else {
        _selectedValues.remove(itemValue);
      }
    });
  }

  void _onCancelTap() {
    Navigator.pop(context);
  }

  void _onSubmitTap() {
    Navigator.pop(context, _selectedValues);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Select Languages'),
      contentPadding: EdgeInsets.only(top: 12.0),
      content: SingleChildScrollView(
        child: ListTileTheme(
          contentPadding: EdgeInsets.fromLTRB(14.0, 0.0, 24.0, 0.0),
          child: ListBody(
            children: widget.items.map(_buildItem).toList(),
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('CANCEL'),
          onPressed: _onCancelTap,
        ),
        FlatButton(
          child: Text('OK'),
          onPressed: _onSubmitTap,
        )
      ],
    );
  }

  Widget _buildItem(MultiSelectDialogItem<V> item) {
    final checked = _selectedValues.contains(item.value);
    return CheckboxListTile(
      value: checked,
      title: Text(item.label),
      controlAffinity: ListTileControlAffinity.leading,
      onChanged: (checked) => _onItemCheckedChange(item.value, checked),
    );
  }
}
