package com.cti.artistarmy.common;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManagement {
  // LogCat tag
  private static String TAG = SessionManagement.class.getSimpleName();
  // Shared Preferences
  SharedPreferences pref;
  SharedPreferences.Editor editor;
  Context _context;
  // Shared pref mode
  int PRIVATE_MODE = 0;
  // Shared preferences file name
  private static final String PREF_NAME = "Artist_Army";
  private static final String KEY_USERNAME = "username";
  private static final String KEY_OTP = "otp";
  private static final String KEY_ID = "id";
  private static final String KEY_IMAGE = "profile_image";
  private static final String KEY_USERID = "id";
  private static final String KEY_Song1 = "song1";
  private static final String KEY_Song2 = "song2";
  private static final String KEY_Song3 = "song2";
  private static final String Email = "email";
  public final static String LOGIN_STATUS = "false";
  public final static String USER_TYPE = "";
  public final static String DEVICE_ID = "";
  public final static String Donate_bits = "";


  public SessionManagement(Context context) {
    this._context = context;
    pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    editor = pref.edit();
  }

  public void setKEY_USERNAME(String s) {
    editor.putString("KEY_USERNAME", s);
    editor.commit();
  }
  public String getKEY_USERNAME() {
    return pref.getString("KEY_USERNAME", KEY_USERNAME);
  }



  public void setNameOfRec(String s) {
    editor.putString("NAME_OF_REC", s);
    editor.commit();
  }
  public void setEmail(String Email) {
    editor.putString("Email", Email);
    editor.commit();
  }
  public String getEmail() {
    return pref.getString("Email", Email);
  }


  public void setKEY_USERID(String s) {
    editor.putString("KEY_USERID", s);
    editor.commit();
  }

  public String getKEY_USERID() {
    return pref.getString("KEY_USERID", KEY_USERID);
  }


  public void setKEY_ID(String s) {
    editor.putString("KEY_ID", s);
    editor.commit();
  }

  public String getKEY_ID() {
    return pref.getString("KEY_ID", KEY_ID);
  }




  public void setKeyImage(String s) {
    editor.putString("profile_image", s);
    editor.commit();
  }

  public String getKeyImage() {
    return pref.getString("profile_image", KEY_IMAGE);
  }

  public void setUSER_TYPE(String s) {
    editor.putString("USER_TYPE", s);
    editor.commit();
  }
  public String getUSER_TYPE() {
    return pref.getString("USER_TYPE", USER_TYPE);
  }
  public void setLoginStatus(String s) {
    editor.putString("LOGIN_STATUS", s);
    editor.commit();
  }
  public String getLoginStatus() {
    return pref.getString("LOGIN_STATUS", LOGIN_STATUS);
  }


  public void setDEVICE_ID(String s) {
    editor.putString("DEVICE_ID", s);
    editor.commit();
  }
  public String getDEVICE_ID() {
    return pref.getString("DEVICE_ID", DEVICE_ID);
  }
  public void setDonate_bits(String s) {
    editor.putString("Donate_bits", s);
    editor.commit();
  }
  public String getDonate_bits() {
    return pref.getString("Donate_bits", Donate_bits);
  }


}
