package com.example.blynderdating.utils

import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.example.blynderdating.R

class UIUtility {

   fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            } else {
                TODO("VERSION.SDK_INT < M")
            }
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }


     fun showDialog(title: String,context: Context) {
         val dialog = Dialog(context)
         dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
         dialog.setCancelable(false)
         dialog.setContentView(R.layout.custom_alert_error)
         val linear_close = dialog.findViewById(R.id.linear_close) as LinearLayout
         val txt_message = dialog.findViewById(R.id.txt_message) as TextView
         txt_message.text=title
         linear_close.setOnClickListener { dialog.dismiss() }
         dialog.show()

     }


    fun toast_error(message: String?,context: Context) {
        val toast = Toast(context)
        val view: View = LayoutInflater.from(context).inflate(R.layout.custom_alert_error, null)
        val textView = view.findViewById(R.id.txt_message) as TextView
        textView.text = message
        toast.setView(view)
        toast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 50)
        toast.setDuration(Toast.LENGTH_LONG)
        toast.show()
    }


    fun toast_success(message: String?,context: Context) {
        val toast = Toast(context)
        val view: View = LayoutInflater.from(context).inflate(R.layout.custom_alert_success, null)
        val textView = view.findViewById(R.id.txt_message) as TextView
        textView.text = message
        toast.setView(view)
        toast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 50)
        toast.setDuration(Toast.LENGTH_LONG)
        toast.show()
    }


    fun toast_try_again(message: String?,context: Context) {
        val toast = Toast(context)
        val view: View = LayoutInflater.from(context).inflate(R.layout.custom_alert_try_again, null)
        val textView = view.findViewById(R.id.txt_message) as TextView
        textView.text = message
        toast.setView(view)
        toast.setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, 50)
        toast.setDuration(Toast.LENGTH_LONG)
        toast.show()
    }

}