package com.cti.artistarmy.Network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class APIClient {
private static Retrofit retrofit = null;
 public static Retrofit getClient() {
   HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
   interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
   OkHttpClient client = new OkHttpClient.Builder()
           .addInterceptor(interceptor).build();
   OkHttpClient.Builder builder = new OkHttpClient.Builder();
   builder.connectTimeout(5, TimeUnit.MINUTES) // connect timeout
           .writeTimeout(5, TimeUnit.MINUTES) // write timeout
           .readTimeout(5, TimeUnit.MINUTES); // read timeout

   client = builder.build();

   if (retrofit == null) {
     retrofit = new Retrofit.Builder()
             .baseUrl("https://ctinfotech.com/CTCC/artist_army/api/")
             .addConverterFactory(GsonConverterFactory.create())
             .client(client)
             .build();
   }  return retrofit;
 }
}


