package com.cti.artistarmy.Network;

import com.cti.artistarmy.model.AddComentResponse;
import com.cti.artistarmy.model.Comment;
import com.cti.artistarmy.model.CommentResponse;
import com.cti.artistarmy.model.GenreList;
import com.cti.artistarmy.model.PostList;
import com.cti.artistarmy.model.ReelsResponse;
import com.cti.artistarmy.model.SignupModel;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {
    //Signup
    @FormUrlEncoded
    @POST("signup_mobile")
    Call<SignupModel> signup(@Field("mobile_number") String mobile_number,
                             @Field("country_code") String country_code,
                             @Field("social") String social);
 //Otp Verification
    @FormUrlEncoded
    @POST("otp_verification")
    Call<SignupModel> otp_verification(@Field("otp") String otp,
                             @Field("mobile_number") String mobile_number);
    //Set Password
    @FormUrlEncoded
    @POST("signup_pass")
    Call<SignupModel> set_password(@Field("password") String password,
                                       @Field("mobile_number") String mobile_number);
    //login
    @FormUrlEncoded
    @POST("login")
    Call<SignupModel> login(@Field("password") String password,
                                   @Field("mobile_number") String mobile_number);
    //login
    @FormUrlEncoded
    @POST("post_like")
    Call<SignupModel> like(@Field("user_id") String user_id,
                            @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("post_list")
    Call<PostList> getPostList(@Field("user_id") String user_id);

    @GET("genre_category")
    Call<GenreList> getGenreList();

    @Multipart
    @POST("add_post")
    Call<SignupModel> uploadImages( @Part("user_id") RequestBody user_id,
                                     @Part("description") RequestBody description,
                                     @Part("post_type") RequestBody post_type,
                                     @Part List<MultipartBody.Part> images);



   @GET("reels_list")
   Call<ReelsResponse> getReelList();



    @FormUrlEncoded
    @POST("post_all_comments")
    Call<CommentResponse> getCommentList(
            @Field("post_id") String post_id,
            @Field("user_id") String user_id
    );


    @FormUrlEncoded
    @POST("add_post_comment")
    Call<AddComentResponse> addComment(
            @Field("post_id") String post_id,
            @Field("sender_id") String user_id,
            @Field("message") String message
    );
}
